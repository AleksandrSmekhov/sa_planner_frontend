import axios, { type CreateAxiosDefaults } from 'axios';

import { authService } from '@/services/auth.service';
import {
	getAccessToken,
	removeFromStorage
} from '@/services/auth-token.service';
import { errorCatch } from './error';

const options: CreateAxiosDefaults = {
	baseURL: process.env.NEXT_PUBLIC_BASE_URL,
	headers: {
		'Content-Type': 'application/json'
	},
	withCredentials: true
};

const axiosNoAuth = axios.create(options);

const axiosWithAuth = axios.create(options);

axiosWithAuth.interceptors.request.use(config => {
	const accessToken = getAccessToken();

	if (config?.headers && accessToken)
		config.headers.Authorization = `Bearer ${accessToken}`;

	return config;
});

axiosWithAuth.interceptors.response.use(
	config => config,
	async error => {
		const originalRequest = error.config;
		const errorText = errorCatch(error);

		if (
			(error?.response?.status === 401 ||
				errorText === 'jwt expired' ||
				errorText === 'jwt must be provided' ||
				errorText === 'Refresh token not passed') &&
			error.config &&
			!error.config._isRetry
		) {
			originalRequest._isRetry = true;
			try {
				await authService.getNewTokens();
				return axiosWithAuth.request(originalRequest);
			} catch (error) {
				if (errorCatch(error) === 'jwt expired') removeFromStorage();
			}
		}

		throw error;
	}
);

export { axiosNoAuth, axiosWithAuth };
