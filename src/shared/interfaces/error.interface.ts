export enum ErrorTexts {
	wrongPassword = 'Invalid password',
	noUser = 'User not found',
	shortPassword = 'password must be at least 6 characters long',
	wrongEmail = 'email must be an email',
	userExists = 'User already exists',
	tooMuchIntervals = 'settings.intervalsCount must not be greater than 10',
	lessIntervals = 'settings.intervalsCount must not be less than 1'
}

export interface IRequestError {
	message: ErrorTexts | ErrorTexts[];
	error: string;
	statusCode: number;
}
