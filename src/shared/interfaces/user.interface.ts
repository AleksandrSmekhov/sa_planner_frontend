interface IUserSettings {
	intervalsCount: number;
	workInterval: number;
	breakInterval: number;
}

export interface IUser {
	id: string;
	email: string;

	name?: string;
	userSettings?: IUserSettings;
}

export interface IUserStatistics {
	totalTasks: number;
	completedTasks: number;
	todayTasks: number;
	weekTasks: number;
}

export type TypeUserForm = Omit<IUser, 'id' | 'userSettings'> & {
	password?: string;
} & IUserSettings;

export type TypeUpdateUser = Partial<
	Omit<IUser, 'id' | 'userSettings'> & {
		password: string;
	} & { settings: IUserSettings }
>;
