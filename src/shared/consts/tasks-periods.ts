import dayjs, { type Dayjs } from 'dayjs';
import isoWeek from 'dayjs/plugin/isoWeek';
import weekOfYear from 'dayjs/plugin/weekOfYear';

dayjs.extend(isoWeek);
dayjs.extend(weekOfYear);

export type TypePeriodValues =
	| 'today'
	| 'tomorrow'
	| 'on-this-week'
	| 'on-next-week'
	| 'later';

interface IColumn {
	label: string;
	value: TypePeriodValues | 'completed';
}

type TypeFilter = Record<TypePeriodValues, Dayjs>;

export const FILTERS: TypeFilter = {
	today: dayjs().startOf('day'),
	tomorrow: dayjs().add(1, 'day').startOf('day'),
	'on-this-week': dayjs().endOf('isoWeek'),
	'on-next-week': dayjs().add(1, 'week').startOf('day'),
	later: dayjs().add(2, 'week').startOf('day')
};

export const COLUMNS: IColumn[] = [
	{
		label: 'Сегодня',
		value: 'today'
	},
	{
		label: 'Завтра',
		value: 'tomorrow'
	},
	{
		label: 'На этой неделе',
		value: 'on-this-week'
	},
	{
		label: 'На следующей неделе',
		value: 'on-next-week'
	},
	{
		label: 'Позже',
		value: 'later'
	},
	{
		label: 'Выполнено',
		value: 'completed'
	}
];
