class EventKeys {
	BACKSPACE = 'Backspace';
	TAB = 'Tab';
	ENTER = 'Enter';
	ARROW_LEFT = 'ArrowLeft';
	ARROW_RIGHT = 'ArrowRight';

	notControlButton = (key: string) => {
		return (
			key !== this.BACKSPACE &&
			key !== this.TAB &&
			key !== this.ENTER &&
			key !== this.ARROW_LEFT &&
			key !== this.ARROW_RIGHT
		);
	};
}

export const EVENT_KEYS = new EventKeys();
