class DASHBOARD {
	private root = `${process.env.BASE_URL || ''}/lk`;

	AUTH = `${process.env.BASE_URL || ''}/auth`;
	HOME = this.root;
	TASKS = `${this.root}/tasks`;
	TIMER = `${this.root}/timer`;
	TIME_BLOCKING = `${this.root}/time-blocking`;
	SETTINGS = `${this.root}/settings`;
}

export const DASHBOARD_PAGES = new DASHBOARD();
