export enum EnumTimeBlockColors {
	tomato = 'tomato',
	orchid = 'orchid',
	coral = 'coral',
	seagreen = 'seagreen',
	orange = 'orange',
	royalblue = 'royalblue',
	lightslategray = 'lightslategray'
}

export const TIME_BLOCKS_COLORS = [
	EnumTimeBlockColors.tomato,
	EnumTimeBlockColors.orchid,
	EnumTimeBlockColors.coral,
	EnumTimeBlockColors.seagreen,
	EnumTimeBlockColors.orange,
	EnumTimeBlockColors.royalblue,
	EnumTimeBlockColors.lightslategray
];
