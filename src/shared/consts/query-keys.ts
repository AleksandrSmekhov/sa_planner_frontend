export class QueryKeys {
	UPDATE_PROFILE = 'update profile';
	PROFILE = 'profile';
	AUTH = 'auth';
	TASKS = 'tasks';
	UPDATE_TASK = 'update task';
	CREATE_TASK = 'create task';
	DELETE_TASK = 'delete task';
	UPDATE_ROUND = 'update round';
	TODAY_SESSION = 'today session';
	CREATE_SESSION = 'create session';
	DELETE_SESSION = 'delete session';
	CREATE_TIME_BLOCK = 'create time block';
	UPDATE_TIME_BLOCK = 'update time block';
	DELETE_TIME_BLOCK = 'delete time block';
	UPDATE_TIME_BLOCKS_ORDERS = 'update time blocks order';
	TIME_BLOCKS = 'time-blocks';
}

export const QUERY_KEYS = new QueryKeys();
