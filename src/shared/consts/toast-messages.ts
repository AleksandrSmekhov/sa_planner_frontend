class Messages {
	SUCCESS_LOGIN = 'Вы успешно вошли в систему';
	ERROR_WRONG_PASSWORD = 'Неверный пароль';
	ERROR_USER_NOT_FOUND = 'Пользователь не найден';
	ERROR_USER_EXISTS = 'Пользователь уже существует';
	ERROR_WRONG_EMAIL_FORMAT = "Неверный формат email'а";
	ERROR_SHORT_PASSWORD = 'Пароль должен быть минимум 6 символов';
	ERROR_UNKNOWN = 'Произошла ошибка';
	ERROR_TOO_MUCH_INTERVALS = 'Количество интервалов не может быть больше 10';
	ERROR_SMALL_INTERVALS = 'Количество интервалов должно быть больше 0';
	SUCCESS_UPDATE_PROFILE = 'Данные профиля обновлены';
}

export const TOAST_MESSAGES = new Messages();
