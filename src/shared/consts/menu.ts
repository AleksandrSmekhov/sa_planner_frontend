import CalendarRange from '@/shared/assets/icons/calendar-range.svg';
import KanbanSquare from '@/shared/assets/icons/square-kanban.svg';
import LayoutDashboard from '@/shared/assets/icons/layout-dashboard.svg';
import Settings from '@/shared/assets/icons/settings.svg';
import Timer from '@/shared/assets/icons/timer.svg';

import { DASHBOARD_PAGES } from './pages-urls';

export interface IMenuItem {
	link: string;
	name: string;
	icon: React.FC<React.SVGProps<SVGSVGElement>>;
}

export const MENU: IMenuItem[] = [
	{
		link: DASHBOARD_PAGES.HOME,
		name: 'Статистика',
		icon: LayoutDashboard
	},
	{
		link: DASHBOARD_PAGES.TASKS,
		name: 'Задачи',
		icon: KanbanSquare
	},
	{
		link: DASHBOARD_PAGES.TIMER,
		name: 'Помодоро',
		icon: Timer
	},
	{
		link: DASHBOARD_PAGES.SETTINGS,
		name: 'Настройки',
		icon: Settings
	},
	{
		link: DASHBOARD_PAGES.TIME_BLOCKING,
		name: 'Расчёт времени',
		icon: CalendarRange
	}
];
