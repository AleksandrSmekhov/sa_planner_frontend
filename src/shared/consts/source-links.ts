import Gitlab from '@/shared/assets/icons/gitlab.svg';

interface ISourceLink {
	link: string;
	label: string;
	icon?: React.FC<React.SVGProps<SVGSVGElement>>;
}

export const SOURCE_LINKS: ISourceLink[] = [
	{ label: 'На главную', link: 'https://smekhov-alex.ru/' },
	{
		label: 'Backend',
		link: 'https://gitlab.com/AleksandrSmekhov/sa_planner_backend/',
		icon: Gitlab
	},
	{
		label: 'Frontend',
		link: 'https://gitlab.com/AleksandrSmekhov/sa_planner_frontend/',
		icon: Gitlab
	}
];
