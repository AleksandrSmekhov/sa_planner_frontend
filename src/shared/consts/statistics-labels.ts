import type { IUserStatistics } from '../interfaces/user.interface';

export const STATISTICS_LABELES: Record<keyof IUserStatistics, string> = {
	completedTasks: 'Выполненные задачи',
	todayTasks: 'Задач на сегодня',
	totalTasks: 'Общее кол-во задач',
	weekTasks: 'Задач на неделю'
};
