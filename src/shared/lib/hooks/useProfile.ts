import { useQuery } from '@tanstack/react-query';

import { userService } from '@/services/user.service';

import { QUERY_KEYS } from '@/shared/consts/query-keys';

export const useProfile = () => {
	const { data, isLoading, isSuccess } = useQuery({
		queryKey: [QUERY_KEYS.PROFILE],
		queryFn: () => userService.getProfile()
	});

	return { data, isLoading, isSuccess };
};
