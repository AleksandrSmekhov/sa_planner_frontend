import { useQueryClient, useMutation } from '@tanstack/react-query';
import { toast } from 'sonner';

import type { AxiosError } from 'axios';

import { userService } from '@/services/user.service';

import { sendError } from '../utils';

import type { TypeUserForm } from '@/shared/interfaces/user.interface';
import type { IRequestError } from '@/shared/interfaces/error.interface';

import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';
import { QUERY_KEYS } from '@/shared/consts/query-keys';

export const useUpdateSettings = () => {
	const queryClient = useQueryClient();

	const { mutate, isPending } = useMutation({
		mutationKey: [QUERY_KEYS.UPDATE_PROFILE],
		mutationFn: (data: TypeUserForm) =>
			userService.update({
				email: data.email,
				name: data.name,
				password: data.password,
				settings: {
					breakInterval: data.breakInterval,
					intervalsCount: data.intervalsCount,
					workInterval: data.workInterval
				}
			}),
		onSuccess() {
			toast.success(TOAST_MESSAGES.SUCCESS_UPDATE_PROFILE);
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.PROFILE] });
		},
		onError(error: AxiosError<IRequestError>) {
			sendError(error);
		}
	});

	return { mutate, isPending };
};
