import { useEffect } from 'react';
import type { UseFormReset } from 'react-hook-form';

import { useProfile } from '.';

import type { TypeUserForm } from '@/shared/interfaces/user.interface';

export const useInitialData = (reset: UseFormReset<TypeUserForm>) => {
	const { data, isSuccess } = useProfile();

	useEffect(() => {
		if (isSuccess && data)
			reset({
				email: data.user.email,
				name: data.user.name,
				breakInterval: data.user.userSettings?.breakInterval,
				intervalsCount: data.user.userSettings?.intervalsCount,
				workInterval: data.user.userSettings?.workInterval
			});
	}, [isSuccess]);
};
