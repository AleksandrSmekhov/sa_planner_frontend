import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { taskService } from '@/services/task.service';

import type { TypeTaskFormState } from '@/shared/interfaces/task.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

interface IMutationFnProps {
	id: string;
	data: TypeTaskFormState;
}

export const useUpdateTasks = (key?: string) => {
	const queryClient = useQueryClient();

	const { mutate: updateTask } = useMutation({
		mutationKey: [QUERY_KEYS.UPDATE_TASK, key],
		mutationFn: ({ id, data }: IMutationFnProps) =>
			taskService.updateTask(id, data),
		onSuccess() {
			queryClient.invalidateQueries({
				queryKey: [QUERY_KEYS.TASKS]
			});
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { updateTask };
};
