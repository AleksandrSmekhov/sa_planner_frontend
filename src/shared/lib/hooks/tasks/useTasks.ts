import { useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

import { taskService } from '@/services/task.service';

import type { ITaskResponse } from '@/shared/interfaces/task.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';

export const useTasks = () => {
	const { data } = useQuery({
		queryKey: [QUERY_KEYS.TASKS],
		queryFn: () => taskService.getTasks()
	});

	const [tasks, setTasks] = useState<ITaskResponse[] | undefined>(data);

	useEffect(() => {
		setTasks(data);
	}, [data]);

	return { tasks, setTasks };
};
