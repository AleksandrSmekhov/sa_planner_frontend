import { useCallback, useEffect } from 'react';
import { UseFormWatch } from 'react-hook-form';

import { useCreateTask, useDebounce, useUpdateTasks } from '..';

import type { TypeTaskFormState } from '@/shared/interfaces/task.interface';

interface IUserTaskDebounceProps {
	watch: UseFormWatch<TypeTaskFormState>;
	itemId: string;
}

export const useTaskDebounce = ({ watch, itemId }: IUserTaskDebounceProps) => {
	const { createTask } = useCreateTask();
	const { updateTask } = useUpdateTasks();

	const debouncedCreateTask = useCallback(
		useDebounce((formData: TypeTaskFormState) => {
			createTask(formData);
		}, 444),
		[]
	);

	const debouncedUpdateTask = useCallback(
		useDebounce((formData: TypeTaskFormState) => {
			updateTask({ data: formData, id: itemId });
		}, 444),
		[]
	);

	useEffect(() => {
		const { unsubscribe } = watch(formData => {
			if (itemId) {
				debouncedUpdateTask({
					...formData,
					priority: formData.priority || undefined
				});
			} else {
				debouncedCreateTask(formData);
			}
		});

		return () => {
			unsubscribe();
		};
	}, [watch(), debouncedUpdateTask, debouncedCreateTask]);
};
