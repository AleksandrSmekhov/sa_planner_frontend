import type { DropResult } from '@hello-pangea/dnd';

import { useUpdateTasks } from '.';

import { FILTERS, type TypePeriodValues } from '@/shared/consts/tasks-periods';

export const useTaskDnd = () => {
	const { updateTask } = useUpdateTasks();

	const onDragEnd = (result: DropResult) => {
		if (!result.destination) return;

		const destinationColumnId = result.destination.droppableId;

		if (destinationColumnId === result.source.droppableId) return;

		if (destinationColumnId === 'completed') {
			updateTask({
				id: result.draggableId,
				data: {
					isCompleted: true
				}
			});
			return;
		}

		const newCreatedAt =
			FILTERS[destinationColumnId as TypePeriodValues].format();

		updateTask({
			id: result.draggableId,
			data: {
				createdAt: newCreatedAt,
				isCompleted: false
			}
		});
	};

	return { onDragEnd };
};
