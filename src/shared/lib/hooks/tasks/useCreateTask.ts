import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { taskService } from '@/services/task.service';

import type { TypeTaskFormState } from '@/shared/interfaces/task.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export const useCreateTask = () => {
	const queryClient = useQueryClient();

	const { mutate: createTask } = useMutation({
		mutationKey: [QUERY_KEYS.CREATE_TASK],
		mutationFn: (data: TypeTaskFormState) => taskService.createTask(data),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TASKS] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { createTask };
};
