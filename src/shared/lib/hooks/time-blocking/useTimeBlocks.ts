import { useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

import { timeBlockService } from '@/services/time-block.service';

import type { ITimeBlockResponse } from '@/shared/interfaces/time-block.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';

export const useTimeBlocks = () => {
	const { data, isLoading } = useQuery({
		queryKey: [QUERY_KEYS.TIME_BLOCKS],
		queryFn: () => timeBlockService.getTimeBlocks()
	});

	const [timeBlocks, setTimeBlocks] = useState<
		ITimeBlockResponse[] | undefined
	>(data);

	useEffect(() => {
		setTimeBlocks(data);
	}, [data]);

	return { timeBlocks, setTimeBlocks, isLoading };
};
