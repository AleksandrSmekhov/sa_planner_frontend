import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { timeBlockService } from '@/services/time-block.service';

import type { TypeTimeBlockFormState } from '@/shared/interfaces/time-block.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export const useCreateTimeBlock = () => {
	const queryClient = useQueryClient();

	const { mutate: createTimeBlock, isPending } = useMutation({
		mutationKey: [QUERY_KEYS.CREATE_TIME_BLOCK],
		mutationFn: (data: TypeTimeBlockFormState) =>
			timeBlockService.createTimeBlock(data),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TIME_BLOCKS] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { createTimeBlock, isPending };
};
