import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { timeBlockService } from '@/services/time-block.service';

import type { TypeTimeBlockFormState } from '@/shared/interfaces/time-block.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

interface IMutationProps {
	id: string;
	data: TypeTimeBlockFormState;
}

export const useUpdateTimeBlock = (key?: string) => {
	const queryClient = useQueryClient();

	const { mutate: updateTimeBlock } = useMutation({
		mutationKey: [QUERY_KEYS.UPDATE_TIME_BLOCK, key],
		mutationFn: ({ data, id }: IMutationProps) =>
			timeBlockService.updateTimeBlock(id, data),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TIME_BLOCKS] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { updateTimeBlock };
};
