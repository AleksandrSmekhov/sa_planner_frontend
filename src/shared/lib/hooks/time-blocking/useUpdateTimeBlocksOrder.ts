import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { timeBlockService } from '@/services/time-block.service';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export const useUpdateTimeBlockOrder = () => {
	const queryClient = useQueryClient();

	const { mutate: updateTimeBlocksOrder } = useMutation({
		mutationKey: [QUERY_KEYS.UPDATE_TIME_BLOCKS_ORDERS],
		mutationFn: (ids: string[]) => timeBlockService.updateOrderTimeBlock(ids),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TIME_BLOCKS] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { updateTimeBlocksOrder };
};
