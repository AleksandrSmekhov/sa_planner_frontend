import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { timeBlockService } from '@/services/time-block.service';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export function useDeleteTimeBlock(timeBlockId: string) {
	const queryClient = useQueryClient();

	const { mutate: deleteTimeBlock, isPending: isDeletePending } = useMutation({
		mutationKey: [QUERY_KEYS.DELETE_TIME_BLOCK, timeBlockId],
		mutationFn: () => timeBlockService.deleteTimeBlock(timeBlockId),
		onSuccess() {
			queryClient.invalidateQueries({
				queryKey: [QUERY_KEYS.TIME_BLOCKS]
			});
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { deleteTimeBlock, isDeletePending };
}
