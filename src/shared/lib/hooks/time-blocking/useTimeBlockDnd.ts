import {
	useSensors,
	useSensor,
	PointerSensor,
	KeyboardSensor,
	type DragEndEvent
} from '@dnd-kit/core';
import { arrayMove } from '@dnd-kit/sortable';

import { useUpdateTimeBlockOrder } from './useUpdateTimeBlocksOrder';

import type { ITimeBlockResponse } from '@/shared/interfaces/time-block.interface';

interface IUseTimeBlockDndProps {
	timeBlocks: ITimeBlockResponse[] | undefined;
	setTimeBlocks: React.Dispatch<
		React.SetStateAction<ITimeBlockResponse[] | undefined>
	>;
}

export const useTimeBlockDnd = ({
	setTimeBlocks,
	timeBlocks
}: IUseTimeBlockDndProps) => {
	const sensors = useSensors(
		useSensor(PointerSensor),
		useSensor(KeyboardSensor)
	);

	const { updateTimeBlocksOrder } = useUpdateTimeBlockOrder();

	const handleDragEnd = (event: DragEndEvent) => {
		const { active, over } = event;

		if (active.id !== over?.id && timeBlocks) {
			const oldIndex = timeBlocks.findIndex(item => item.id === active.id);
			const newIndex = timeBlocks.findIndex(
				item => item.id === (over?.id || '')
			);

			if (oldIndex !== -1 && newIndex !== -1) {
				const newTimeBlocks = arrayMove(timeBlocks, oldIndex, newIndex);
				setTimeBlocks(newTimeBlocks);
				updateTimeBlocksOrder(newTimeBlocks.map(timeBlock => timeBlock.id));
			}
		}
	};

	return { handleDragEnd, sensors };
};
