import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { pomodoroService } from '@/services/pomodoro.service';

import type { TypePomdoroRoundState } from '@/shared/interfaces/pomodoro.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

interface IMutationFnProps {
	id: string;
	data: TypePomdoroRoundState;
}

export const useUpdateRound = () => {
	const queryClient = useQueryClient();

	const { mutate: updateRound, isPending: isUpdateRoundPending } = useMutation({
		mutationKey: [QUERY_KEYS.UPDATE_ROUND],
		mutationFn: ({ id, data }: IMutationFnProps) =>
			pomodoroService.updateRound(id, data),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TODAY_SESSION] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { updateRound, isUpdateRoundPending };
};
