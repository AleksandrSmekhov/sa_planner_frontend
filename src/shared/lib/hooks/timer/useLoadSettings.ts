import { useProfile } from '..';

export const useLoadSettings = () => {
	const { data } = useProfile();

	const workInterval = data?.user.userSettings?.workInterval ?? 50;
	const breakInterval = data?.user.userSettings?.breakInterval ?? 10;

	return { workInterval, breakInterval };
};
