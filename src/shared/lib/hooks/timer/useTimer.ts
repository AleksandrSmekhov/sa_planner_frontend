import { useState, useEffect } from 'react';

import { useLoadSettings } from './useLoadSettings';

import type { IPomodoroRoundResponse } from '@/shared/interfaces/pomodoro.interface';
import type { IBaseTimerProps } from './baseTimerProps.interface';

export const useTimer = (): IBaseTimerProps => {
	const { breakInterval, workInterval } = useLoadSettings();

	const [isRunning, setIsRunning] = useState<boolean>(false);
	const [isBreakTime, setIsBreakTime] = useState<boolean>(false);
	const [secondsLeft, setSecondsLeft] = useState<number>(workInterval * 60);
	const [activeRound, setActiveRound] = useState<IPomodoroRoundResponse>();

	useEffect(() => {
		let interval: NodeJS.Timeout | null = null;

		if (isRunning) {
			interval = setInterval(() => {
				setSecondsLeft(secondsLeft => secondsLeft - 1);
			}, 1000);
		} else if (!isRunning && secondsLeft !== 0 && interval) {
			clearInterval(interval);
		}

		return () => {
			if (interval) clearInterval(interval);
		};
	}, [isRunning, secondsLeft, workInterval, activeRound]);

	useEffect(() => {
		if (secondsLeft > 0) return;

		setIsBreakTime(!isBreakTime);
		setSecondsLeft((isBreakTime ? workInterval : breakInterval) * 60);
	}, [secondsLeft, isBreakTime, workInterval, breakInterval]);

	return {
		activeRound,
		isRunning,
		secondsLeft,
		setActiveRound,
		setIsRunning,
		setSecondsLeft
	};
};
