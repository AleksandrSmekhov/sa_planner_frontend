import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { pomodoroService } from '@/services/pomodoro.service';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export const useDeleteSession = (onDeleteSuccess: () => void) => {
	const queryClient = useQueryClient();

	const { mutate: deleteSession, isPending: isDeletePending } = useMutation({
		mutationKey: [QUERY_KEYS.DELETE_SESSION],
		mutationFn: (id: string) => pomodoroService.deleteSession(id),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TODAY_SESSION] });
			onDeleteSuccess();
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { deleteSession, isDeletePending };
};
