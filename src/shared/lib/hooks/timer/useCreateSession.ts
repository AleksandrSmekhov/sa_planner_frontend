import { useMutation, useQueryClient } from '@tanstack/react-query';
import { toast } from 'sonner';

import { pomodoroService } from '@/services/pomodoro.service';

import { QUERY_KEYS } from '@/shared/consts/query-keys';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

export const useCreateSession = () => {
	const queryClient = useQueryClient();

	const { mutate, isPending } = useMutation({
		mutationKey: [QUERY_KEYS.CREATE_SESSION],
		mutationFn: () => pomodoroService.createSession(),
		onSuccess() {
			queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.TODAY_SESSION] });
		},
		onError() {
			toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
		}
	});

	return { mutate, isPending };
};
