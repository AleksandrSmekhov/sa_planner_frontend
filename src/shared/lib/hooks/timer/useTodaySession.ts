import { useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';

import { useLoadSettings } from './useLoadSettings';

import { pomodoroService } from '@/services/pomodoro.service';

import type { IBaseTimerProps } from './baseTimerProps.interface';

import { QUERY_KEYS } from '@/shared/consts/query-keys';

export const useTodaySession = ({
	setActiveRound,
	setSecondsLeft
}: IBaseTimerProps) => {
	const { workInterval } = useLoadSettings();

	const {
		data: sessionsResponse,
		isLoading,
		isSuccess
	} = useQuery({
		queryKey: [QUERY_KEYS.TODAY_SESSION],
		queryFn: () => pomodoroService.getTodaySession()
	});

	const rounds = sessionsResponse?.rounds;

	useEffect(() => {
		if (isSuccess && rounds) {
			const activeRound = rounds.find(round => !round.isCompleted);
			setActiveRound(activeRound);

			if (activeRound && activeRound.totalSeconds !== 0) {
				setSecondsLeft(activeRound.totalSeconds);
			}
		}
	}, [isSuccess, rounds]);

	return { sessionsResponse, isLoading, workInterval };
};
