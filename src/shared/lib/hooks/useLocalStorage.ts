import { useState, useRef, useEffect } from 'react';

interface IUseLocalStorageProps<T> {
	key: string;
	defaultValue: T;
}

export const useLocalStorage = <T>({
	defaultValue,
	key
}: IUseLocalStorageProps<T>): [
	T,
	React.Dispatch<React.SetStateAction<T>>,
	boolean
] => {
	const [isLoading, setIsLoading] = useState<boolean>(true);

	const isMounted = useRef(false);
	const [value, setValue] = useState<T>(defaultValue);

	useEffect(() => {
		try {
			const item = localStorage.getItem(key);
			if (item) {
				setValue(JSON.parse(item));
			}
		} catch (e) {
			console.log(e);
		} finally {
			setIsLoading(false);
		}

		return () => {
			isMounted.current = false;
		};
	}, [key]);

	useEffect(() => {
		if (isMounted.current) {
			localStorage.setItem(key, JSON.stringify(value));
		} else {
			isMounted.current = true;
		}
	}, [key, value]);

	return [value, setValue, isLoading];
};
