import { toast } from 'sonner';

import type { AxiosError } from 'axios';

import type { IRequestError } from '@/shared/interfaces/error.interface';

import { ErrorTexts } from '@/shared/interfaces/error.interface';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';

const sendErrorMessage = (text: ErrorTexts) => {
	switch (text) {
		case ErrorTexts.wrongPassword:
			return toast.error(TOAST_MESSAGES.ERROR_WRONG_PASSWORD);
		case ErrorTexts.noUser:
			return toast.error(TOAST_MESSAGES.ERROR_USER_NOT_FOUND);
		case ErrorTexts.userExists:
			return toast.error(TOAST_MESSAGES.ERROR_USER_EXISTS);
		case ErrorTexts.wrongEmail:
			return toast.error(TOAST_MESSAGES.ERROR_WRONG_EMAIL_FORMAT);
		case ErrorTexts.shortPassword:
			return toast.error(TOAST_MESSAGES.ERROR_SHORT_PASSWORD);
		case ErrorTexts.tooMuchIntervals:
			return toast.error(TOAST_MESSAGES.ERROR_TOO_MUCH_INTERVALS);
		case ErrorTexts.lessIntervals:
			return toast.error(TOAST_MESSAGES.ERROR_SMALL_INTERVALS);
		default:
			return toast.error(TOAST_MESSAGES.ERROR_UNKNOWN);
	}
};

export const sendError = (error: AxiosError<IRequestError>) => {
	if (!error.response) return;

	if (Array.isArray(error.response.data.message)) {
		error.response.data.message.forEach(message => sendErrorMessage(message));
		return;
	}

	sendErrorMessage(error.response.data.message);
};
