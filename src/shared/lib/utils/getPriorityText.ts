import { EnumTaskPriority } from '@/shared/interfaces/task.interface';

export const getPriorityText = (priority: EnumTaskPriority) => {
	if (priority === EnumTaskPriority.low) return 'Низкий приоритет';
	if (priority === EnumTaskPriority.medium) return 'Средний приоритет';

	return 'Высокий приоритет';
};
