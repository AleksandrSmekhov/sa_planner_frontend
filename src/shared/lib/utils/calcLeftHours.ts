import type { ITimeBlockResponse } from '@/shared/interfaces/time-block.interface';

export const calcLeftHours = (timeBlocks: ITimeBlockResponse[] | undefined) => {
	const totalMinutes =
		timeBlocks?.reduce((result, timeBlock) => result + timeBlock.duration, 0) ||
		0;
	const totalHours = Math.floor(totalMinutes / 60);
	const hoursLeft = 24 - totalHours;

	return hoursLeft;
};
