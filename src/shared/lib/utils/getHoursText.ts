const A_ENDING_NUMBERS = [2, 3, 4];

export const getHoursText = (number: number) => {
	if (number === 1 || (number > 20 && number % 10 === 1)) return 'час остался';
	if (
		A_ENDING_NUMBERS.includes(number) ||
		(number > 20 && A_ENDING_NUMBERS.includes(number % 10))
	)
		return 'часа осталось';
	return 'часов осталось';
};
