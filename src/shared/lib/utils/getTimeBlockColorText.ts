import { EnumTimeBlockColors } from '@/shared/consts/time-blocking-color';

export const getTimeBlockColorText = (color: EnumTimeBlockColors) => {
	switch (color) {
		case EnumTimeBlockColors.coral:
			return 'Коралловый';
		case EnumTimeBlockColors.tomato:
			return 'Томатный';
		case EnumTimeBlockColors.orange:
			return 'Оранжевый';
		case EnumTimeBlockColors.seagreen:
			return 'Цвет морской волны';
		case EnumTimeBlockColors.orchid:
			return 'Cветло-лиловый';
		case EnumTimeBlockColors.lightslategray:
			return 'Светло серый';
		case EnumTimeBlockColors.royalblue:
			return 'Королевский синий';
	}
};
