'use client';

import { useState } from 'react';
import { useRouter } from 'next/navigation';
import Link from 'next/link';
import { useMutation } from '@tanstack/react-query';
import { useForm } from 'react-hook-form';
import { toast } from 'sonner';

import type { AxiosError } from 'axios';
import type { SubmitHandler } from 'react-hook-form';

import { Heading } from '@/components/ui/Heading';
import { Field } from '@/components/ui/fields';
import { Button } from '@/components/ui/buttons';

import { authService } from '@/services/auth.service';

import { sendError } from '@/shared/lib/utils';

import type { IAuthForm } from '@/shared/interfaces/auth.interface';
import type { IRequestError } from '@/shared/interfaces/error.interface';

import { FormTypes } from '@/shared/interfaces/auth.interface';
import { DASHBOARD_PAGES } from '@/shared/consts/pages-urls';
import { TOAST_MESSAGES } from '@/shared/consts/toast-messages';
import { SOURCE_LINKS } from '@/shared/consts/source-links';
import { QUERY_KEYS } from '@/shared/consts/query-keys';

import cls from './Auth.module.scss';

export function Auth() {
	const [isLoginForm, setIsLoginForm] = useState<boolean>(false);
	const { push } = useRouter();
	const { register, handleSubmit, reset, setError } = useForm<IAuthForm>({
		mode: 'onChange'
	});

	const { mutate } = useMutation({
		mutationKey: [QUERY_KEYS.AUTH],
		mutationFn: (data: IAuthForm) =>
			authService.main(
				isLoginForm ? FormTypes.login : FormTypes.register,
				data
			),
		onSuccess() {
			toast.success(TOAST_MESSAGES.SUCCESS_LOGIN);
			reset();
			push(DASHBOARD_PAGES.HOME);
		},
		onError(error: AxiosError<IRequestError>) {
			sendError(error);
		}
	});

	const onSubmit: SubmitHandler<IAuthForm> = data => {
		mutate(data);
	};

	return (
		<div className={cls.formWrapper}>
			<form onSubmit={handleSubmit(onSubmit)} className={cls.form}>
				<Heading title="Авторизация" />
				<Field
					id="email"
					label="E-mail:"
					placeholder="Введите e-mail"
					type="email"
					extra={cls.email}
					autoComplete="email"
					{...register('email', { required: 'E-mail обязателен!' })}
				/>
				<Field
					id="password"
					label="Пароль:"
					placeholder="Введите пароль"
					type="password"
					extra={cls.password}
					autoComplete="current-password"
					{...register('password', { required: 'Пароль обязателен!' })}
				/>
				<div className={cls.buttons}>
					<Button onClick={() => setIsLoginForm(true)}>Войти</Button>
					<Button onClick={() => setIsLoginForm(false)}>Регистрация</Button>
				</div>
				<div className={cls.links}>
					{SOURCE_LINKS.map(values => (
						<Link href={values.link} className={cls.link} key={values.link}>
							{values.icon ? <values.icon /> : ''}
							{values.label}
						</Link>
					))}
				</div>
			</form>
		</div>
	);
}
