import type { Metadata } from 'next';

import { Auth } from './ui/Auth';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';

export const metadata: Metadata = {
	title: 'Авторизация',
	...NO_INDEX_PAGE
};

export default function AuthPage() {
	return <Auth />;
}
