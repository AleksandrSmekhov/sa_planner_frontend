import type { Metadata } from 'next';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';
import { Settings } from './ui/Settings';
import { Heading } from '@/components/ui/Heading';

export const metadata: Metadata = {
	title: 'Настройки',
	...NO_INDEX_PAGE
};

export default function SettingsPage() {
	return (
		<div>
			<Heading title="Настройки" />
			<Settings />
		</div>
	);
}
