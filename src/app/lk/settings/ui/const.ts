import type { TypeUserForm } from '@/shared/interfaces/user.interface';

import cls from './Settings.module.scss';

interface IField {
	id: keyof TypeUserForm;
	label: string;
	placeholder: string;
	extraClass: string;
	type?: string;
}

export const PRESONAL_FIELDS: IField[] = [
	{
		id: 'email',
		label: 'E-mail:',
		extraClass: cls.notLastItem,
		placeholder: 'Введите e-mail',
		type: 'email'
	},
	{
		id: 'name',
		label: 'Имя:',
		extraClass: cls.notLastItem,
		placeholder: 'Введите имя'
	},
	{
		id: 'password',
		label: 'Пароль:',
		extraClass: cls.lastItem,
		placeholder: 'Введите пароль',
		type: 'password'
	}
];

export const POMODORO_FIELDS: IField[] = [
	{
		id: 'workInterval',
		label: 'Рабочий интервал:',
		extraClass: cls.notLastItem,
		placeholder: 'Введите рабочий интервал (мин.)'
	},
	{
		id: 'breakInterval',
		label: 'Отдых:',
		extraClass: cls.notLastItem,
		placeholder: 'Введите время отдыха (мин.)'
	},
	{
		id: 'intervalsCount',
		label: 'Количество интервалов (макс. 10):',
		extraClass: cls.notLastItem,
		placeholder: 'Введите количество интервалов (макс. 10)'
	}
];
