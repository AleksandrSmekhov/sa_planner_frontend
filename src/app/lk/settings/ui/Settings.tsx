'use client';

import { type SubmitHandler, useForm } from 'react-hook-form';

import { useInitialData, useUpdateSettings } from '@/shared/lib/hooks';

import { Field } from '@/components/ui/fields';
import { Button } from '@/components/ui/buttons';

import type { TypeUserForm } from '@/shared/interfaces/user.interface';

import { PRESONAL_FIELDS, POMODORO_FIELDS } from './const';

import cls from './Settings.module.scss';

export const Settings = () => {
	const { register, handleSubmit, reset } = useForm<TypeUserForm>({
		mode: 'onChange'
	});

	useInitialData(reset);
	const { isPending, mutate } = useUpdateSettings();

	const onSubmit: SubmitHandler<TypeUserForm> = data => {
		const { password, ...rest } = data;

		mutate({
			...rest,
			password: password || undefined
		});
	};

	return (
		<div>
			<form className={cls.settingsForm} onSubmit={handleSubmit(onSubmit)}>
				<div className={cls.settingsBlocks}>
					<div>
						{PRESONAL_FIELDS.map(values => (
							<Field
								key={values.id}
								id={values.id}
								label={values.label}
								placeholder={values.placeholder}
								type={values.type}
								autoComplete="off"
								extra={values.extraClass}
								{...register(values.id)}
							/>
						))}
					</div>
					<div>
						{POMODORO_FIELDS.map(values => (
							<Field
								key={values.id}
								id={values.id}
								label={values.label}
								placeholder={values.placeholder}
								isNumber
								autoComplete="off"
								extra={values.extraClass}
								{...register(values.id, { valueAsNumber: true })}
							/>
						))}
					</div>
				</div>
				<Button type="submit" disabled={isPending}>
					Сохранить
				</Button>
			</form>
		</div>
	);
};
