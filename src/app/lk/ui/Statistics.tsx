'use client';

import { useProfile } from '@/shared/lib/hooks';

import { Loader } from '@/components/ui/loader';

import type { IUserStatistics } from '@/shared/interfaces/user.interface';

import { STATISTICS_LABELES } from '@/shared/consts/statistics-labels';

import cls from './Statistics.module.scss';

export const Statistics = () => {
	const { data, isLoading } = useProfile();

	return isLoading ? (
		<div className={cls.loader}>
			<Loader />
		</div>
	) : data?.statistics ? (
		<div className={cls.statsBlock}>
			{Object.keys(data.statistics).map(key => (
				<div key={key} className={cls.stat}>
					<div className={cls.title}>
						{STATISTICS_LABELES[key as keyof IUserStatistics]}
					</div>
					<div className={cls.value}>
						{data.statistics[key as keyof IUserStatistics]}
					</div>
				</div>
			))}
		</div>
	) : (
		<div className={cls.noStats}>Статистика не найдена...</div>
	);
};
