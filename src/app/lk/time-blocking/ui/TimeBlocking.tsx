'use client';

import { FormProvider, useForm } from 'react-hook-form';

import {
	TimeBlockingForm,
	TimeBlockingsList
} from '@/components/time-blocking';

import type { TypeTimeBlockFormState } from '@/shared/interfaces/time-block.interface';

import cls from './TimeBlocking.module.scss';

export const TimeBlocking = () => {
	const methods = useForm<TypeTimeBlockFormState>();

	return (
		<FormProvider {...methods}>
			<div className={cls.TimeBlocking}>
				<TimeBlockingsList />
				<TimeBlockingForm />
			</div>
		</FormProvider>
	);
};
