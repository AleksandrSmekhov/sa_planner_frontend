import type { Metadata } from 'next';

import { Heading } from '@/components/ui/Heading';
import { TimeBlocking } from './ui/TimeBlocking';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';

export const metadata: Metadata = {
	title: 'Расчёт времени',
	...NO_INDEX_PAGE
};

export default function TimeBlockingPage() {
	return (
		<div>
			<Heading title="Расчёт времени" />
			<TimeBlocking />
			<div></div>
		</div>
	);
}
