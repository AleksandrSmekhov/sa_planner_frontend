'use client';

import {
	useCreateSession,
	useDeleteSession,
	useTimer,
	useTimerActions,
	useTodaySession
} from '@/shared/lib/hooks';

import Play from '@/shared/assets/icons/play.svg';
import Pause from '@/shared/assets/icons/pause.svg';
import Refresh from '@/shared/assets/icons/refresh.svg';
import { Loader } from '@/components/ui/loader';
import { PomoroRounds } from '@/components/pomoro-rounds';
import { Button } from '@/components/ui/buttons';

import { formatTime } from '@/shared/lib/utils';

import cls from './Pomodor.module.scss';

export const Pomodoro = () => {
	const timerState = useTimer();

	const { isLoading, workInterval, sessionsResponse } =
		useTodaySession(timerState);
	const rounds = sessionsResponse?.rounds;
	const timerActions = useTimerActions({
		...timerState,
		rounds: rounds
	});
	const { deleteSession, isDeletePending } = useDeleteSession(() =>
		timerState.setSecondsLeft(workInterval * 60)
	);
	const { isPending, mutate } = useCreateSession();

	return (
		<div className={cls.pomodoro}>
			{!isLoading && (
				<div className={cls.time}>{formatTime(timerState.secondsLeft)}</div>
			)}
			{isLoading ? (
				<div style={{ height: '11.5rem', width: '20rem' }}>
					<Loader />
				</div>
			) : sessionsResponse ? (
				<>
					<PomoroRounds
						activeRound={timerState.activeRound}
						nextRoundHandler={timerActions.nextRoundHandler}
						prevRoundHandler={timerActions.prevRoundHandler}
						rounds={rounds}
					/>
					<button
						className={cls.pause}
						onClick={
							timerState.isRunning
								? timerActions.pauseHandler
								: timerActions.playHandler
						}
						disabled={timerActions.isUpdateRoundPending}
					>
						{timerState.isRunning ? <Pause /> : <Play />}
					</button>
					<button
						className={cls.refresh}
						onClick={() => {
							timerState.setIsRunning(false);
							deleteSession(sessionsResponse.id);
						}}
						disabled={isDeletePending}
					>
						<Refresh />
					</button>
				</>
			) : (
				<Button
					onClick={() => mutate()}
					disabled={isPending}
					className={cls.startButton}
				>
					Запустить таймер
				</Button>
			)}
		</div>
	);
};
