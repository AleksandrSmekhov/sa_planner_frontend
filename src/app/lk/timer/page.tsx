import type { Metadata } from 'next';

import { Heading } from '@/components/ui/Heading';
import { Pomodoro } from './ui/Pomodoro';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';

export const metadata: Metadata = {
	title: 'Таймер',
	...NO_INDEX_PAGE
};

export default function TimerPage() {
	return (
		<div>
			<Heading title="Таймер" />
			<Pomodoro />
		</div>
	);
}
