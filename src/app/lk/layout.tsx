import type { PropsWithChildren } from 'react';

import { DashboardLayout } from '@/components/dashboard-layout';

export default function Layout({ children }: PropsWithChildren) {
	return <DashboardLayout>{children}</DashboardLayout>;
}
