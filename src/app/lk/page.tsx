import type { Metadata } from 'next';

import { Heading } from '@/components/ui/Heading';
import { Statistics } from './ui/Statistics';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';

export const metadata: Metadata = {
	title: 'Статистика',
	...NO_INDEX_PAGE
};

export default function DashboardPage() {
	return (
		<div>
			<Heading title="Статистика" />
			<Statistics />
		</div>
	);
}
