import type { Metadata } from 'next';

import { Heading } from '@/components/ui/Heading';
import { TasksView } from './ui/TasksView';

import { NO_INDEX_PAGE } from '@/shared/consts/seo';

export const metadata: Metadata = {
	title: 'Задачи',
	...NO_INDEX_PAGE
};

export default function TasksPage() {
	return (
		<div>
			<Heading title="Задачи" />
			<TasksView />
		</div>
	);
}
