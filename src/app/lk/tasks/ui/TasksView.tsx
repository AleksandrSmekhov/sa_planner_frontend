'use client';

import { useLocalStorage } from '@/shared/lib/hooks';

import { ViewSwitcher } from '@/components/view-switcher';
import { ListView } from '@/components/list-view';
import { KanbanView } from '@/components/kanban-view';
import { Loader } from '@/components/ui/loader';

import { EnumTasksView } from '@/shared/consts/tasks-type';

export const TasksView = () => {
	const [view, setView, isLoading] = useLocalStorage<EnumTasksView>({
		key: 'task-view',
		defaultValue: EnumTasksView.list
	});

	if (isLoading)
		return (
			<div style={{ height: 'calc(100dvh - 8.8rem)' }}>
				<Loader />
			</div>
		);

	return (
		<div>
			<ViewSwitcher setView={setView} view={view} />
			{view === EnumTasksView.list ? <ListView /> : <KanbanView />}
		</div>
	);
};
