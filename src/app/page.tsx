import { redirect } from 'next/navigation';

import { DASHBOARD_PAGES } from '@/shared/consts/pages-urls';

export default function Home() {
	redirect(DASHBOARD_PAGES.HOME);
}
