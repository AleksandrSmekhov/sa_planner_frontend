import { Noto_Sans } from 'next/font/google';
import { Toaster } from 'sonner';

import type { Metadata } from 'next';

import { Providers } from './providers';

import { SITE_NAME } from '@/shared/consts/seo';

import './styles/globals.scss';

const zen = Noto_Sans({
	subsets: ['cyrillic', 'latin'],
	weight: ['300', '400', '500', '600', '700'],
	display: 'swap',
	variable: '--font-zen',
	style: ['normal']
});

export const metadata: Metadata = {
	title: {
		default: SITE_NAME,
		template: `%s | ${SITE_NAME}`
	},
	description: 'Планировщик своего дня и недели.',
	icons: [
		{
			rel: 'apple-touch-icon',
			sizes: '180x180',
			url: `${process.env.BASE_URL || ''}/apple-touch-icon.png`
		},
		{
			rel: 'icon',
			sizes: '32x32',
			type: 'image/png',
			url: `${process.env.BASE_URL || ''}/favicon-32x32.png`
		},
		{
			rel: 'icon',
			type: 'image/png',
			sizes: '16x16',
			url: `${process.env.BASE_URL || ''}/favicon-16x16.png`
		},
		{
			rel: 'manifest',
			url: `${process.env.BASE_URL || ''}/site.webmanifest`
		},
		{
			rel: 'shortcut icon',
			type: 'image/x-icon',
			url: `${process.env.BASE_URL || ''}/favicon.ico`
		}
	]
};

export default function RootLayout({
	children
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="ru">
			<body className={zen.className}>
				<Providers>
					{children}
					<Toaster theme="dark" position="bottom-right" duration={1500} />
				</Providers>
			</body>
		</html>
	);
}
