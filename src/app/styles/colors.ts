class Colors {
	PRIMARY = '#7551ff';
	SECONDARY = '#f97912';
	BORDER = '#ffffff1f';
	SIDEBAR = '#141515';
	BG = '#0e0f0f';
}

export const COLORS = new Colors();
