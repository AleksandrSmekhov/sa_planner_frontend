import { DragDropContext } from '@hello-pangea/dnd';

import { useTaskDnd, useTasks } from '@/shared/lib/hooks';

import cls from './KanbanView.module.scss';
import { COLUMNS } from '@/shared/consts/tasks-periods';
import { KanbanColumn } from '../kanban-column';

export const KanbanView = () => {
	const { setTasks, tasks } = useTasks();
	const { onDragEnd } = useTaskDnd();

	return (
		<DragDropContext onDragEnd={onDragEnd}>
			<div className={cls.kanbanView}>
				{COLUMNS.map(column => (
					<KanbanColumn
						items={tasks}
						label={column.label}
						setItems={setTasks}
						value={column.value}
						key={column.value}
					/>
				))}
			</div>
		</DragDropContext>
	);
};
