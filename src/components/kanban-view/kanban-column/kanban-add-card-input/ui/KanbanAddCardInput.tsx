import type { ITaskResponse } from '@/shared/interfaces/task.interface';

import cls from './KanbanAddCardInput.module.scss';

interface IKanbanAddCardnputProps {
	filterDate?: string;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const KanbanAddCardInput = ({
	setItems,
	filterDate
}: IKanbanAddCardnputProps) => {
	const addCard = () => {
		setItems(prev => {
			if (!prev) return undefined;
			return [
				...prev,
				{
					id: '',
					name: '',
					isCompleted: false,
					createdAt: filterDate
				}
			];
		});
	};

	return (
		<div className={cls.addCard}>
			<button onClick={addCard}>Добавить задачу...</button>
		</div>
	);
};
