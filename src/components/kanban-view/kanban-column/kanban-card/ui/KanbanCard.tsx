import { Controller, useForm } from 'react-hook-form';

import { useDeleteTask, useTaskDebounce } from '@/shared/lib/hooks';

import GripVertical from '@/shared/assets/icons/grip-vertical.svg';
import Trash from '@/shared/assets/icons/trash.svg';
import Loader from '@/shared/assets/icons/loader.svg';
import { Checkbox } from '@/components/ui/checkbox';
import { TransparentField } from '@/components/ui/fields';
import { DatePicker, SingleSelect } from '@/components/ui/task-edit';

import { getPriorityText } from '@/shared/lib/utils';

import type {
	ITaskResponse,
	TypeTaskFormState
} from '@/shared/interfaces/task.interface';

import { EnumTaskPriority } from '@/shared/interfaces/task.interface';

import cls from './KanbanCard.module.scss';

interface IKanbanCardProps {
	item: ITaskResponse;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const KanbanCard = ({ item, setItems }: IKanbanCardProps) => {
	const { deleteTask, isDeletePending } = useDeleteTask();
	const { register, control, watch } = useForm<TypeTaskFormState>({
		defaultValues: {
			name: item.name,
			isCompleted: item.isCompleted,
			createdAt: item.createdAt,
			priority: item.priority
		}
	});

	useTaskDebounce({ watch, itemId: item.id });

	return (
		<div
			className={`${cls.kanbanCard} ${
				watch('isCompleted') ? cls.completed : ''
			}`}
		>
			<div className={cls.cardHeader}>
				<button aria-describedby="todo-item">
					<GripVertical className={cls.grip} />
				</button>
				<Controller
					control={control}
					name="isCompleted"
					render={({ field: { value, onChange } }) => (
						<Checkbox onChange={onChange} checked={value} />
					)}
				/>
				<TransparentField {...register('name')} autoComplete="off" />
			</div>
			<div className={cls.cardBody}>
				<Controller
					control={control}
					name="createdAt"
					render={({ field: { value, onChange } }) => (
						<DatePicker
							onChange={onChange}
							value={value || ''}
							position="left"
						/>
					)}
				/>

				<Controller
					control={control}
					name="priority"
					render={({ field: { value, onChange } }) => (
						<SingleSelect
							onChange={onChange}
							value={value || ''}
							options={Object.keys(EnumTaskPriority).map(key => ({
								value: key,
								label: getPriorityText(key as EnumTaskPriority)
							}))}
						/>
					)}
				/>
			</div>
			<div className={cls.cardActions}>
				<button
					onClick={() =>
						item.id ? deleteTask(item.id) : setItems(prev => prev?.slice(0, -1))
					}
					className={cls.deleteButton}
				>
					{isDeletePending ? <Loader width={15} height={15} /> : <Trash />}
				</button>
			</div>
		</div>
	);
};
