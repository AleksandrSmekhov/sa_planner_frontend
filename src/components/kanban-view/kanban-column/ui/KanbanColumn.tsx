import { Droppable, Draggable } from '@hello-pangea/dnd';

import { KanbanCard } from '../kanban-card';
import { KanbanAddCardInput } from '../kanban-add-card-input';

import { filterTasks } from '@/shared/lib/utils';

import type { ITaskResponse } from '@/shared/interfaces/task.interface';
import type { TypePeriodValues } from '@/shared/consts/tasks-periods';

import { FILTERS } from '@/shared/consts/tasks-periods';

import cls from './KanbanColumn.module.scss';

interface IKanbanColumnProps {
	value: TypePeriodValues | 'completed';
	label: string;
	items: ITaskResponse[] | undefined;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const KanbanColumn = ({
	items,
	label,
	setItems,
	value
}: IKanbanColumnProps) => {
	return (
		<Droppable droppableId={value}>
			{provider => (
				<div ref={provider.innerRef} {...provider.droppableProps}>
					<div className={cls.kanbanColumn}>
						<div>{label}</div>
						{filterTasks(value, items)?.map((item, index) => (
							<Draggable key={item.id} draggableId={item.id} index={index}>
								{provided => (
									<div
										ref={provided.innerRef}
										{...provided.draggableProps}
										{...provided.dragHandleProps}
									>
										<KanbanCard key={item.id} item={item} setItems={setItems} />
									</div>
								)}
							</Draggable>
						))}
						{provider.placeholder}
						{value !== 'completed' && !items?.some(item => !item.id) && (
							<KanbanAddCardInput
								setItems={setItems}
								filterDate={
									FILTERS[value] ? FILTERS[value].format() : undefined
								}
							/>
						)}
					</div>
				</div>
			)}
		</Droppable>
	);
};
