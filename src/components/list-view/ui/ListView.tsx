import { DragDropContext } from '@hello-pangea/dnd';

import { useTaskDnd } from '@/shared/lib/hooks';
import { useTasks } from '@/shared/lib/hooks';

import { ListRowParent } from '../list-row-parent';

import { COLUMNS } from '@/shared/consts/tasks-periods';

import cls from './ListView.module.scss';

export const ListView = () => {
	const { tasks, setTasks } = useTasks();
	const { onDragEnd } = useTaskDnd();

	return (
		<DragDropContext onDragEnd={onDragEnd}>
			<div className={cls.listView}>
				<div className={cls.listViewHeader}>
					<div>Название</div>
					<div>До даты</div>
					<div>Приоритет</div>
					<div></div>
				</div>
				<div>
					{COLUMNS.map(column => (
						<ListRowParent
							items={tasks}
							label={column.label}
							value={column.value}
							setItems={setTasks}
							key={column.value}
						/>
					))}
				</div>
			</div>
		</DragDropContext>
	);
};
