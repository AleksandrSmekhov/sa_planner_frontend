import { Controller, useForm } from 'react-hook-form';

import { useDeleteTask, useTaskDebounce } from '@/shared/lib/hooks';

import Trash from '@/shared/assets/icons/trash.svg';
import Loader from '@/shared/assets/icons/loader.svg';
import GripVertical from '@/shared/assets/icons/grip-vertical.svg';
import { Checkbox } from '@/components/ui/checkbox';
import { TransparentField } from '@/components/ui/fields';
import { DatePicker, SingleSelect } from '@/components/ui/task-edit';

import { getPriorityText } from '@/shared/lib/utils';

import type {
	ITaskResponse,
	TypeTaskFormState
} from '@/shared/interfaces/task.interface';

import { EnumTaskPriority } from '@/shared/interfaces/task.interface';
import { COLORS } from '@/app/styles/colors';

import cls from './ListRow.module.scss';

interface IListRowProps {
	item: ITaskResponse;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const ListRow = ({ item, setItems }: IListRowProps) => {
	const { deleteTask, isDeletePending } = useDeleteTask();
	const { register, control, watch } = useForm<TypeTaskFormState>({
		defaultValues: {
			createdAt: item.createdAt,
			isCompleted: item.isCompleted,
			name: item.name,
			priority: item.priority
		}
	});

	useTaskDebounce({ watch, itemId: item.id });

	return (
		<div
			className={`${cls.listRow} ${watch('isCompleted') ? cls.completed : ''}`}
		>
			<div>
				<span className={cls.name}>
					<button aria-describedby="todo-item">
						<GripVertical className={cls.grip} />
					</button>
					<Controller
						control={control}
						name="isCompleted"
						render={({ field: { value, onChange } }) => (
							<Checkbox onChange={onChange} checked={value} />
						)}
					/>
					<TransparentField {...register('name')} autoComplete="off" />
				</span>
			</div>
			<div style={{ borderWidth: 0 }}>
				<Controller
					control={control}
					name="createdAt"
					render={({ field: { value, onChange } }) => (
						<DatePicker onChange={onChange} value={value || ''} />
					)}
				/>
			</div>
			<div style={{ borderLeft: `1px solid ${COLORS.BORDER}` }}>
				<Controller
					control={control}
					name="priority"
					render={({ field: { value, onChange } }) => (
						<SingleSelect
							onChange={onChange}
							value={value || ''}
							options={Object.keys(EnumTaskPriority).map(key => ({
								value: key,
								label: getPriorityText(key as EnumTaskPriority)
							}))}
						/>
					)}
				/>
			</div>
			<div>
				<button
					onClick={() =>
						item.id ? deleteTask(item.id) : setItems(prev => prev?.slice(0, -1))
					}
					className={cls.deleteButton}
				>
					{isDeletePending ? <Loader width={15} height={15} /> : <Trash />}
				</button>
			</div>
		</div>
	);
};
