import type { ITaskResponse } from '@/shared/interfaces/task.interface';

import cls from './ListAddRowInput.module.scss';

interface IListAddRowInputProps {
	filterDate?: string;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const ListAddRowInput = ({
	setItems,
	filterDate
}: IListAddRowInputProps) => {
	const addRow = () => {
		setItems(prev => {
			if (!prev) return undefined;
			return [
				...prev,
				{
					id: '',
					name: '',
					isCompleted: false,
					createdAt: filterDate
				}
			];
		});
	};

	return (
		<div className={cls.addRow}>
			<button onClick={addRow}>Добавить задачу...</button>
		</div>
	);
};
