import { Droppable, Draggable } from '@hello-pangea/dnd';

import { ListRow } from '../list-row';
import { ListAddRowInput } from '../list-add-row-input';

import { filterTasks } from '@/shared/lib/utils';

import type { ITaskResponse } from '@/shared/interfaces/task.interface';
import type { TypePeriodValues } from '@/shared/consts/tasks-periods';

import { FILTERS } from '@/shared/consts/tasks-periods';

import cls from './ListRowParent.module.scss';

interface IListRowParentProps {
	value: TypePeriodValues | 'completed';
	label: string;
	items: ITaskResponse[] | undefined;
	setItems: React.Dispatch<React.SetStateAction<ITaskResponse[] | undefined>>;
}

export const ListRowParent = ({
	items,
	label,
	setItems,
	value
}: IListRowParentProps) => {
	return (
		<Droppable droppableId={value}>
			{provider => (
				<div ref={provider.innerRef} {...provider.droppableProps}>
					<div className={cls.RowParentHeading}>
						<div>{label}</div>
					</div>

					{filterTasks(value, items)?.map((item, index) => (
						<Draggable key={item.id} draggableId={item.id} index={index}>
							{provided => (
								<div
									ref={provided.innerRef}
									{...provided.draggableProps}
									{...provided.dragHandleProps}
								>
									<ListRow key={item.id} item={item} setItems={setItems} />
								</div>
							)}
						</Draggable>
					))}

					{provider.placeholder}
					{value !== 'completed' && !items?.some(item => !item.id) && (
						<ListAddRowInput
							setItems={setItems}
							filterDate={FILTERS[value] ? FILTERS[value].format() : undefined}
						/>
					)}
				</div>
			)}
		</Droppable>
	);
};
