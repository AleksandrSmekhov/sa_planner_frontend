import { useFormContext } from 'react-hook-form';

import { useDeleteTimeBlock, useTimeBlockSortable } from '@/shared/lib/hooks';

import Grip from '@/shared/assets/icons/grip-vertical.svg';
import Trash from '@/shared/assets/icons/trash.svg';
import Loader from '@/shared/assets/icons/loader.svg';
import Edit from '@/shared/assets/icons/edit.svg';

import type {
	ITimeBlockResponse,
	TypeTimeBlockFormState
} from '@/shared/interfaces/time-block.interface';

import { EnumTimeBlockColors } from '@/shared/consts/time-blocking-color';

import cls from './TimeBlock.module.scss';

interface ITimeBlockProps {
	timeBlock: ITimeBlockResponse;
}

export const TimeBlock = ({ timeBlock }: ITimeBlockProps) => {
	const { attributes, listeners, setNodeRef, style } = useTimeBlockSortable(
		timeBlock.id
	);

	const { reset } = useFormContext<TypeTimeBlockFormState>();

	const { deleteTimeBlock, isDeletePending } = useDeleteTimeBlock(timeBlock.id);

	const handleReset = () => {
		reset({
			id: timeBlock.id,
			color: timeBlock.color,
			duration: timeBlock.duration,
			name: timeBlock.name,
			order: timeBlock.order
		});
	};

	const handleDelete = () => {
		deleteTimeBlock();
	};

	return (
		<div ref={setNodeRef} style={style}>
			<div
				className={cls.timeBlock}
				style={{
					height: `${timeBlock.duration}px`,
					backgroundColor: timeBlock.color || EnumTimeBlockColors.lightslategray
				}}
			>
				<div className={cls.name}>
					<button {...attributes} {...listeners} aria-describedby="time-block">
						<Grip className={cls.grip} />
					</button>
					<div>
						{timeBlock.name}{' '}
						<i className={cls.minText}>({timeBlock.duration} мин.)</i>
					</div>
				</div>

				<div className={cls.buttons}>
					<button
						onClick={handleReset}
						className={`${cls.button} ${cls.reset}`}
					>
						{' '}
						<Edit width={16} height={16} viewBox="0 0 24 24" />
					</button>
					<button className={cls.button} onClick={handleDelete}>
						{isDeletePending ? (
							<Loader width={16} height={16} />
						) : (
							<Trash width={16} height={16} />
						)}
					</button>
				</div>
			</div>
		</div>
	);
};
