import { DndContext, closestCenter } from '@dnd-kit/core';
import {
	SortableContext,
	verticalListSortingStrategy
} from '@dnd-kit/sortable';

import { useTimeBlocks, useTimeBlockDnd } from '@/shared/lib/hooks';

import { Loader } from '@/components/ui/loader';
import { TimeBlock } from '../time-block';

import { calcLeftHours, getHoursText } from '@/shared/lib/utils';

import cls from './TimeBlockingsList.module.scss';

export const TimeBlockingsList = () => {
	const { timeBlocks, setTimeBlocks, isLoading } = useTimeBlocks();
	const { handleDragEnd, sensors } = useTimeBlockDnd({
		timeBlocks,
		setTimeBlocks
	});

	if (isLoading)
		return (
			<div style={{ height: 'calc(100dvh - 8.8rem)' }}>
				<Loader />
			</div>
		);

	const hoursLeft = calcLeftHours(timeBlocks);
	const isNoBlocks = !timeBlocks?.length;

	return (
		<div className={isNoBlocks ? cls.noBlocks : ''}>
			<DndContext
				sensors={sensors}
				collisionDetection={closestCenter}
				onDragEnd={handleDragEnd}
			>
				<div>
					<SortableContext
						items={timeBlocks || []}
						strategy={verticalListSortingStrategy}
					>
						{!isNoBlocks ? (
							timeBlocks?.map(timeBlock => (
								<TimeBlock key={timeBlock.id} timeBlock={timeBlock} />
							))
						) : (
							<div className={cls.emptyText}></div>
						)}
					</SortableContext>
				</div>
			</DndContext>
			<div>
				{hoursLeft > 0
					? `${hoursLeft} ${getHoursText(hoursLeft)} из 24 часов на сон.`
					: 'Не осталось часов для сна('}
			</div>
		</div>
	);
};
