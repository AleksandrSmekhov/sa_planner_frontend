import {
	type SubmitHandler,
	useFormContext,
	Controller
} from 'react-hook-form';

import { useCreateTimeBlock, useUpdateTimeBlock } from '@/shared/lib/hooks';
import type { TypeTimeBlockFormState } from '@/shared/interfaces/time-block.interface';

import { TIME_BLOCKS_COLORS } from '@/shared/consts/time-blocking-color';

import cls from './TimeBlockingForm.module.scss';
import { Field } from '@/components/ui/fields';
import { SingleSelect } from '@/components/ui/task-edit';
import { getTimeBlockColorText } from '@/shared/lib/utils';
import { Button } from '@/components/ui/buttons';

export const TimeBlockingForm = () => {
	const { register, control, watch, reset, handleSubmit, getValues } =
		useFormContext<TypeTimeBlockFormState>();

	const existsId = watch('id');

	const { updateTimeBlock } = useUpdateTimeBlock();
	const { createTimeBlock, isPending } = useCreateTimeBlock();

	const onSubmit: SubmitHandler<TypeTimeBlockFormState> = data => {
		const { color, id, ...rest } = data;
		const dto = { ...rest, color: color || undefined };

		if (id) updateTimeBlock({ id, data });
		else createTimeBlock(dto);

		reset({
			color: TIME_BLOCKS_COLORS[TIME_BLOCKS_COLORS.length - 1],
			duration: 0,
			name: '',
			id: undefined,
			order: 1
		});
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)} className={cls.timeBlockingForm}>
			<Field
				{...register('name', { required: true })}
				id="name"
				label="Введите название:"
				placeholder="Введите название"
				extra={cls.formField}
				autoComplete="off"
			/>
			<Field
				{...register('duration', { required: true, valueAsNumber: true })}
				id="duration"
				label="Введите продолжительность (мин.):"
				placeholder="Введите продолжительность (мин.)"
				isNumber
				extra={cls.formField}
				autoComplete="off"
			/>
			<div>
				<span className={cls.formSelect}>Цвет:</span>
				<Controller
					control={control}
					name="color"
					render={({ field: { value, onChange } }) => (
						<SingleSelect
							options={TIME_BLOCKS_COLORS.map(color => ({
								value: color,
								label: getTimeBlockColorText(color)
							}))}
							onChange={onChange}
							value={value || TIME_BLOCKS_COLORS[TIME_BLOCKS_COLORS.length - 1]}
							isColorSelect
						/>
					)}
				/>
			</div>
			<Button type="submit" disabled={isPending} className={cls.submitButton}>
				{existsId ? 'Обновить' : 'Создать'}
			</Button>
		</form>
	);
};
