import ListTodo from '@/shared/assets/icons/list-todo.svg';
import Kanban from '@/shared/assets/icons/kanban.svg';

import { EnumTasksView } from '@/shared/consts/tasks-type';

import cls from './ViewSwitcher.module.scss';

interface IViewSwitcherProps {
	view: EnumTasksView;
	setView: (value: EnumTasksView) => void;
}

export const ViewSwitcher = ({ setView, view }: IViewSwitcherProps) => {
	const handleListSwitch = () => {
		setView(EnumTasksView.list);
	};

	const handleKanbanSwitch = () => {
		setView(EnumTasksView.kanban);
	};

	return (
		<div className={cls.viewSwitcher}>
			<button
				onClick={handleListSwitch}
				className={`${cls.button} ${
					view === EnumTasksView.kanban ? cls.notActive : ''
				}`}
			>
				<ListTodo />
				Список
			</button>
			<button
				onClick={handleKanbanSwitch}
				className={`${cls.button} ${
					view === EnumTasksView.list ? cls.notActive : ''
				}`}
			>
				<Kanban />
				Доска
			</button>
		</div>
	);
};
