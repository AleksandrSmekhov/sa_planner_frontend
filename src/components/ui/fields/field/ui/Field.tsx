import { type HTMLInputAutoCompleteAttribute, forwardRef } from 'react';

import { EVENT_KEYS } from '@/shared/consts/event-keys';

import cls from './Field.module.scss';

interface InputFieldProps {
	id: string;
	label: string;
	placeholder: string;
	variant?: string;
	state?: 'error' | 'success';
	disabled?: boolean;
	type?: string;
	isNumber?: boolean;
	extra?: string;
	autoComplete?: HTMLInputAutoCompleteAttribute;
}

export const Field = forwardRef<HTMLInputElement, InputFieldProps>(
	(
		{
			label,
			id,
			extra,
			type,
			placeholder,
			state,
			disabled,
			isNumber,
			autoComplete,
			...rest
		},
		ref
	) => {
		return (
			<div className={extra}>
				<label htmlFor={id} className={cls.label}>
					{label}
				</label>
				<input
					ref={ref}
					disabled={disabled}
					type={type}
					id={id}
					placeholder={placeholder}
					autoComplete={autoComplete}
					className={`${cls.input} ${
						disabled
							? cls.disabled
							: state === 'error'
							? cls.error
							: state === 'success'
							? cls.success
							: ''
					}`}
					onKeyDown={event => {
						if (
							isNumber &&
							!/[0-9]/.test(event.key) &&
							EVENT_KEYS.notControlButton(event.key)
						)
							event.preventDefault();
					}}
					{...rest}
				/>
			</div>
		);
	}
);

Field.displayName = 'field';
