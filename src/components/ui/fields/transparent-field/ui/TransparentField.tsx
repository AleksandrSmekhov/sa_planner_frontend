import { forwardRef } from 'react';
import cls from './TransparentField.module.scss';

type TypeTransparentFieldProps = React.InputHTMLAttributes<HTMLInputElement>;

export const TransparentField = forwardRef<
	HTMLInputElement,
	TypeTransparentFieldProps
>(({ className, ...rest }, ref) => {
	return (
		<input
			className={`${cls.transparentField} ${className || ''}`}
			ref={ref}
			{...rest}
		/>
	);
});

TransparentField.displayName = 'TransparentField';
