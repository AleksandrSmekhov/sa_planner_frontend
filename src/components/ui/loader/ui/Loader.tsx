import Loading from '@/shared/assets/icons/loader.svg';

import cls from './Loader.module.scss';

export const Loader = () => {
	return (
		<div className={cls.wrapper}>
			<Loading className={cls.icon} />
		</div>
	);
};
