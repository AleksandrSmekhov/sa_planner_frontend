import type { CSSProperties, PropsWithChildren } from 'react';

import { EnumTaskPriority } from '@/shared/interfaces/task.interface';

interface IBadgeProps extends PropsWithChildren {
	className?: string;
	priority?: EnumTaskPriority;
	style?: CSSProperties;
}

export const Badge = ({
	children,
	className,
	style,
	priority
}: IBadgeProps) => {
	return (
		<span
			style={{
				display: 'flex',
				width: 'max-content',
				padding: '0.25rem 0.5rem',
				borderRadius: '0.5rem',
				fontSize: '1rem',
				lineHeight: '1.5rem',
				fontWeight: 600,
				color: '#ffffff',
				transitionProperty:
					'color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter',
				transitionTimingFunction: 'cubic-bezier(0.4, 0, 0.2, 1)',
				transitionDuration: '150ms',
				backgroundColor: priority
					? priority === EnumTaskPriority.low
						? '#60a5fab3'
						: priority === EnumTaskPriority.medium
						? '#fb923cb3'
						: '#f8717199'
					: '#808080',
				...style
			}}
			className={className}
		>
			{children}
		</span>
	);
};
