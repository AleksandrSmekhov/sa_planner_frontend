import Cross from '@/shared/assets/icons/cross.svg';

import { useOutside } from '@/shared/lib/hooks';

import { Badge } from '@/components/ui/Badge';

import type { EnumTaskPriority } from '@/shared/interfaces/task.interface';

import cls from './SingleSelect.module.scss';

export interface IOption {
	label: string;
	value: string;
}

interface ISingleSelectProps {
	options: IOption[];
	onChange: (value: string) => void;
	value: string;
	isColorSelect?: boolean;
}

export const SingleSelect = ({
	options,
	onChange,
	value,
	isColorSelect
}: ISingleSelectProps) => {
	const { isShow, ref, setIsShow } = useOutside(false);
	const getValue = () => options.find(option => option.value === value)?.label;

	const handleClick = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>
	) => {
		event.preventDefault();
		setIsShow(!isShow);
	};

	const handleClear = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>
	) => {
		event.preventDefault();
		onChange('');
	};

	const handleSelectOption = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
		option: IOption
	) => {
		event.preventDefault();
		onChange(option.value);
		setIsShow(false);
	};

	return (
		<div
			ref={ref}
			className={`${cls.singleSelect} ${
				isColorSelect ? cls.colorSelected : ''
			}`}
		>
			<button onClick={handleClick}>
				{getValue() ? (
					<Badge
						priority={value as EnumTaskPriority}
						className={cls.select}
						style={
							isColorSelect
								? {
										backgroundColor: value
								  }
								: {}
						}
					>
						{getValue()}
					</Badge>
				) : (
					<Badge>Нажмите для выбора</Badge>
				)}
			</button>
			{value && (
				<button className={cls.cross} onClick={handleClear}>
					<Cross />
				</button>
			)}
			{isShow && (
				<div className={`slide ${cls.optionsList}`}>
					{options.map(option => (
						<button
							key={option.value}
							onClick={event => handleSelectOption(event, option)}
							className={cls.option}
						>
							<Badge
								priority={option.value as EnumTaskPriority}
								style={
									isColorSelect
										? {
												backgroundColor: option.value
										  }
										: {}
								}
							>
								{option.label}
							</Badge>
						</button>
					))}
				</div>
			)}
		</div>
	);
};
