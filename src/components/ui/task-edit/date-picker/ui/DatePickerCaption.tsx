import dayjs from 'dayjs';
import ruLocale from 'dayjs/locale/ru';
import type { DateFormatter } from 'react-day-picker';

dayjs.locale(ruLocale);

type TypeSeasons = 'winter' | 'spring' | 'summer' | 'autumn';

const seasonEmoji: Record<TypeSeasons, string> = {
	winter: '⛄️',
	spring: '🌸',
	summer: '🌻',
	autumn: '🍂'
};

const getSeason = (month: Date): keyof typeof seasonEmoji => {
	const monthNumber = month.getMonth() + 1;

	if (monthNumber > 2 && monthNumber < 6) return 'spring';
	if (monthNumber > 5 && monthNumber < 9) return 'summer';
	if (monthNumber > 8 && monthNumber < 12) return 'autumn';

	return 'winter';
};

export const formatCaption: DateFormatter = month => {
	const season = getSeason(month);

	return (
		<div style={{ textTransform: 'capitalize' }}>
			<span role="img" aria-label={season} style={{ marginRight: '0.5rem' }}>
				{seasonEmoji[season]}
			</span>
			{dayjs(month).format('MMMM')}
		</div>
	);
};
