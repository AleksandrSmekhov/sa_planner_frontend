import { useState } from 'react';
import dayjs from 'dayjs';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import { DayPicker, type SelectSingleEventHandler } from 'react-day-picker';
import { ru } from 'date-fns/locale';

import Cross from '@/shared/assets/icons/cross.svg';
import { formatCaption } from './DatePickerCaption';

import { useOutside } from '@/shared/lib/hooks';

import 'react-day-picker/dist/style.css';
import './styles/DatePicker.scss';
import cls from './styles/DatePicker.module.scss';

dayjs.extend(LocalizedFormat);

interface IDatePickerProps {
	onChange: (value: string) => void;
	value: string;
	position?: 'left' | 'right';
}

export const DatePicker = ({
	onChange,
	value,
	position = 'right'
}: IDatePickerProps) => {
	const [selected, setSelected] = useState<Date>();
	const { ref, isShow, setIsShow } = useOutside(false);

	const handleDaySelect: SelectSingleEventHandler = date => {
		const ISOdate = date?.toISOString();

		setSelected(date);
		onChange(ISOdate || '');
		if (ISOdate) setIsShow(false);
	};

	const handleClear = () => {
		onChange('');
	};

	return (
		<div className={cls.datePicker} ref={ref}>
			<button onClick={() => setIsShow(!isShow)}>
				{value ? dayjs(value).format('LL') : 'Нажмите для выбора'}
			</button>
			{value && (
				<button className={cls.cross} onClick={handleClear}>
					<Cross />
				</button>
			)}
			{isShow && (
				<div
					className={`${cls.datePickerWrapper} ${
						position === 'left' ? cls.left : cls.right
					} slide`}
				>
					<DayPicker
						fromYear={2024}
						locale={ru}
						toYear={2054}
						initialFocus={isShow}
						mode="single"
						defaultMonth={selected}
						selected={selected}
						onSelect={handleDaySelect}
						weekStartsOn={1}
						formatters={{ formatCaption }}
					/>
				</div>
			)}
		</div>
	);
};
