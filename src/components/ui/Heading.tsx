import { COLORS } from '@/app/styles/colors';

interface IHeadingProps {
	title: string;
}

export const Heading = ({ title }: IHeadingProps) => {
	return (
		<div>
			<h1
				style={{ fontSize: '1.875rem', lineHeight: '2.25rem', fontWeight: 500 }}
			>
				{title}
			</h1>
			<div
				style={{
					margin: '0.75rem 0px',
					height: '0.125rem',
					width: '100%',
					backgroundColor: COLORS.BORDER
				}}
			></div>
		</div>
	);
};
