import cls from './Checkbox.module.scss';

interface ICheckboxProps {
	id?: string;
	extra?: string;
	[rest: string]: any;
}

export const Checkbox = ({ extra, id, ...rest }: ICheckboxProps) => {
	return (
		<input
			id={id}
			type="checkbox"
			className={`defaultCheckbox ${cls.checkbox} ${extra}`}
			name="weekly"
			{...rest}
		/>
	);
};
