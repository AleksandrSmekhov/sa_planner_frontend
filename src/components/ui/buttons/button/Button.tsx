import type { ButtonHTMLAttributes, PropsWithChildren } from 'react';

import cls from './Button.module.scss';

type TypeButtonProps = PropsWithChildren<
	ButtonHTMLAttributes<HTMLButtonElement>
>;

export const Button = ({ children, className, ...rest }: TypeButtonProps) => {
	return (
		<button className={`${className || ''} ${cls.button}`} {...rest}>
			{children}
		</button>
	);
};
