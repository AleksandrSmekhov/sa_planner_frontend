import ChevronLeft from '@/shared/assets/icons/chevron-left.svg';
import ChevronRight from '@/shared/assets/icons/chevron-right.svg';

import type { IPomodoroRoundResponse } from '@/shared/interfaces/pomodoro.interface';

import cls from './PomoroRounds.module.scss';

interface IPomodoroRoundProps {
	rounds: IPomodoroRoundResponse[] | undefined;
	nextRoundHandler: () => void;
	prevRoundHandler: () => void;
	activeRound: IPomodoroRoundResponse | undefined;
}

export const PomoroRounds = ({
	activeRound,
	nextRoundHandler,
	prevRoundHandler,
	rounds
}: IPomodoroRoundProps) => {
	const isCanPrevRound = rounds
		? rounds.some(round => round.isCompleted)
		: false;
	const isCanNextRound = rounds
		? !rounds[rounds.length - 1].isCompleted
		: false;

	return (
		<div className={cls.pomodoroRounds}>
			<button
				className={cls.button}
				disabled={!isCanPrevRound}
				onClick={() => (isCanPrevRound ? prevRoundHandler() : false)}
			>
				<ChevronLeft width={23} height={23} />
			</button>
			<div className={cls.roundsContainer}>
				{rounds &&
					rounds.map((round, index) => (
						<div
							key={index}
							className={`${cls.round} ${
								round.isCompleted ? cls.completed : ''
							} ${
								round.id === activeRound?.id && !round.isCompleted
									? cls.active
									: ''
							}`}
						></div>
					))}
			</div>
			<button
				className={cls.button}
				disabled={!isCanNextRound}
				onClick={() => (isCanNextRound ? nextRoundHandler() : false)}
			>
				<ChevronRight width={23} height={23} />
			</button>
		</div>
	);
};
