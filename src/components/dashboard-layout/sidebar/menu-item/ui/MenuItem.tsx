import Link from 'next/link';

import type { IMenuItem } from '@/shared/consts/menu';

import cls from './MenuItem.module.scss';

interface IMenuItemProps {
	item: IMenuItem;
}

export const MenuItem = ({
	item: { icon: Icon, link, name }
}: IMenuItemProps) => {
	return (
		<div>
			<Link href={link} className={cls.menuItem}>
				<Icon />
				<span>{name}</span>
			</Link>
		</div>
	);
};
