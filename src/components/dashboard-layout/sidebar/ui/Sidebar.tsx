'use client';

import { useState } from 'react';
import Link from 'next/link';

import Logo from '@/shared/assets/icons/logo.svg';
import { LogoutButton } from '../logout-button';
import { MenuItem } from '../menu-item';

import { MENU } from '@/shared/consts/menu';

import cls from './Sidebar.module.scss';

export const Sidebar = () => {
	const [isOpen, setIsOpen] = useState<boolean>(false);

	const handleOpen = () => {
		setIsOpen(prev => !prev);
	};

	return (
		<aside className={cls.sidebar}>
			<div>
				<div className={cls.logo} onClick={handleOpen}>
					<Logo width={38} height={38} />
					<span className={cls.title}>
						Планировщик<span className={cls.extraText}>Pet project</span>
					</span>
				</div>
				<div className={`${cls.sidebarMenu} ${isOpen ? '' : cls.closed}`}>
					{MENU.map(item => (
						<MenuItem key={item.link} item={item} />
					))}
					<LogoutButton />
				</div>
			</div>
			<footer className={cls.sidebarFooter}>
				2024 &copy;{' '}
				<Link
					href="https://smekhov-alex.ru/"
					draggable={false}
					className={cls.mainLink}
				>
					Smekhov Aleksander
				</Link>
				<br />
				All rights not reserved.
			</footer>
		</aside>
	);
};
