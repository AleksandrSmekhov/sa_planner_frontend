'use client';

import { useRouter } from 'next/navigation';
import { useMutation } from '@tanstack/react-query';

import Logout from '@/shared/assets/icons/log-out.svg';

import { authService } from '@/services/auth.service';

import { DASHBOARD_PAGES } from '@/shared/consts/pages-urls';

import cls from './LogoutButton.module.scss';

export const LogoutButton = () => {
	const router = useRouter();
	const { mutate } = useMutation({
		mutationKey: ['logout'],
		mutationFn: () => authService.logout(),
		onSuccess: () => router.push(DASHBOARD_PAGES.AUTH)
	});

	return (
		<div className={cls.menuItem} onClick={() => mutate()}>
			<Logout />
			<span>Выход</span>
		</div>
	);
};
