import type { PropsWithChildren } from 'react';

import { Sidebar } from '../sidebar';
import { Header } from '../header';

import cls from './DashboardLayout.module.scss';

export const DashboardLayout = ({ children }: PropsWithChildren) => {
	return (
		<div className={cls.wrapper}>
			<Sidebar />
			<main className={cls.main}>
				<Header />
				{children}
			</main>
		</div>
	);
};
