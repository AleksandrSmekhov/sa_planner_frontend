import { GlobalLoader } from './GlobalLoader';
import { Profile } from './profile';

export const Header = () => {
	return (
		<header>
			<GlobalLoader />
			<Profile />
		</header>
	);
};
