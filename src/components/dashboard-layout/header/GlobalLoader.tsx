'use client';

import { useIsFetching, useIsMutating } from '@tanstack/react-query';

import { Loader } from '@/components/ui/loader';

export const GlobalLoader = () => {
	const isMutating = useIsMutating();
	const isFetching = useIsFetching();

	return isMutating || isFetching ? (
		<div
			style={{ position: 'fixed', top: '1.4rem', right: '1.4rem', zIndex: 50 }}
		>
			<Loader />
		</div>
	) : null;
};
