'use client';

import { useProfile } from '@/shared/lib/hooks';

import { Loader } from '@/components/ui/loader';

import cls from './Profile.module.scss';

export const Profile = () => {
	const { data, isLoading } = useProfile();

	return (
		<div className={cls.header}>
			{isLoading ? (
				<Loader />
			) : (
				<div className={cls.info}>
					<div className={cls.infoText}>
						<p className={cls.name}>{data?.user.name}</p>
						<p className={cls.email}>{data?.user.email}</p>
					</div>
					<div className={cls.avatar}>
						{data?.user.name?.charAt(0) || data?.user.email.charAt(0) || 'A'}
					</div>
				</div>
			)}
		</div>
	);
};
