import { type NextRequest, NextResponse } from 'next/server';

import { EnumTokens } from './services/auth-token.service';
import { DASHBOARD_PAGES } from './shared/consts/pages-urls';

export async function middleware(request: NextRequest) {
	const { url, cookies } = request;

	const refreshToken = cookies.get(EnumTokens.REFRESH_TOKEN)?.value;

	const isDashboardPage = url.includes(
		`/${
			DASHBOARD_PAGES.HOME.split('/')[
				DASHBOARD_PAGES.HOME.split('/').length - 1
			]
		}`
	);
	const isAuthPage = url.includes(
		`/${
			DASHBOARD_PAGES.AUTH.split('/')[
				DASHBOARD_PAGES.AUTH.split('/').length - 1
			]
		}`
	);

	if (isAuthPage && refreshToken) {
		return NextResponse.redirect(new URL(DASHBOARD_PAGES.HOME, url));
	}

	if (!refreshToken && isDashboardPage) {
		return NextResponse.redirect(new URL(DASHBOARD_PAGES.AUTH, url));
	}

	return NextResponse.next();
}

export const config = {
	matcher: [
		`${process.env.BASE_URL}/lk/:path*`,
		`${process.env.BASE_URL}/auth/:path*`
	]
};
