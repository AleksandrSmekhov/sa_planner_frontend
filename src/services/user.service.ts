import { axiosWithAuth } from '@/api/interceptors';

import type {
	IUser,
	TypeUpdateUser,
	IUserStatistics
} from '@/shared/interfaces/user.interface';

export interface IProfileResponse {
	user: IUser;
	statistics: IUserStatistics;
}

class UserService {
	private BASE_URL = '/user/profile';

	async getProfile() {
		const response = await axiosWithAuth.get<IProfileResponse>(this.BASE_URL);

		return response.data;
	}

	async update(data: TypeUpdateUser) {
		const response = await axiosWithAuth.put(this.BASE_URL, data);

		return response.data;
	}
}

export const userService = new UserService();
