import { axiosNoAuth } from '@/api/interceptors';
import { removeFromStorage, saveTokenStorage } from './auth-token.service';

import type {
	IAuthResponse,
	IAuthForm,
	FormTypes
} from '@/shared/interfaces/auth.interface';

export const authService = {
	async main(type: FormTypes, data: IAuthForm) {
		const response = await axiosNoAuth.post<IAuthResponse>(
			`/auth/${type}`,
			data
		);

		if (response.data.accessToken) saveTokenStorage(response.data.accessToken);

		return response;
	},

	async getNewTokens() {
		const response = await axiosNoAuth.post<IAuthResponse>(
			`/auth/login/access_token`
		);

		if (response.data.accessToken) saveTokenStorage(response.data.accessToken);

		return response;
	},

	async logout() {
		const response = await axiosNoAuth.post<boolean>(`/auth/logout`);

		if (response.data) removeFromStorage();

		return response;
	}
};
