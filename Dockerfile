FROM node:20.14.0-alpine AS dependencies
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install

FROM node:20.14.0-alpine AS builder
WORKDIR /usr/src/app
COPY . .
COPY --from=dependencies /usr/src/app/node_modules ./node_modules
RUN npm run build

FROM node:20.14.0-alpine AS runner
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/public ./public
COPY --from=builder /usr/src/app/package.json ./package.json
COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY .env ./
COPY --from=builder /usr/src/app/.next ./.next

