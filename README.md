# Frontend pet проекта - «Планировщик»

## .env.example

- NEXT_PUBLIC_BASE_URL - базовый URL для API запросов на backend
- NEXT_PUBLIC_SITE_DOMAIN - домен сайта, для cookie
- BASE_URL - дополнительный базовый URL для frontend'а

## Стек

- React
- Next.js
- React Day Picker
- React Hook Form
- Sass
- Sonner
- TanStack Query
- Pangea DnD
- DnD kit

## Установка

Установка зависимостей:

### `yarn` или `npm install`

Запуск режима отладки:

### `yarn dev` или `npm run dev`

Сборка production версии:

### `yarn build` или `npm run build`

Запуск production версии:

### `yarn start` или `npm run start`

## Страницы

- /auth - Cтраница авторизации
- /lk - Страница статистики
- /lk/tasks - Страница задач
- /lk/timer - Страница таймера Помодоро
- /lk/time-blocking - Страница расчёта времени
- /lk/settings - Страница настроек
