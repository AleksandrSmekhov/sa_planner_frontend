/** @type {import('next').NextConfig} */
const nextConfig = {
	basePath: process.env.BASE_URL || '',
	webpack(config) {
		const fileLoaderRule = config.module.rules.find(rule =>
			rule.test?.test?.('.svg')
		);
		fileLoaderRule.exclude = /\.svg$/i;

		config.module.rules.push({
			test: /\.svg$/i,
			issuer: { and: [/\.(js|ts|md)x?$/] },
			use: ['@svgr/webpack']
		});

		return config;
	}
};

export default nextConfig;
